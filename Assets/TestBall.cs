﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestBall : MonoBehaviour {
    Rigidbody rg;
    public float speed = 5f;
    public GameObject my;
	// Use this for initialization
	void Start () {
        rg = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 vec = transform.forward * speed;
        rg.velocity = vec;
    }
    private void OnTriggerEnter(Collider other)
    {
        Destroy(my);
        if (other.gameObject.tag == "Player")
        {
            //ダメージ処理
            
        }

    }
}
