﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindMaker : MonoBehaviour {
    public GameObject[] MoveObjects;
    public Rigidbody[] rbs;
    public bool rightWind = true;
    public float windPower = 0.05f;
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		for(int i = 0;i < MoveObjects.Length; i++)
        {
            if (rightWind)
            {
                Vector3 vec = new Vector3(windPower,0,0);
                rbs[i].AddForce(vec);
            }
            else
            {
                Vector3 vec = new Vector3(windPower * -1, 0, 0);
                rbs[i].AddForce(vec);
            }
        }
	}
}
