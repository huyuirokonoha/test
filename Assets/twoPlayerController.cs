﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class twoPlayerController : MonoBehaviour {
    float speed = 0f;
    int Dashcount = 0;
    float DcomTime = 0f;
    float dashspd = 3f;
    float jumppow;
    float attack;
    bool dash = false;
    Rigidbody rg;
    public bool debug;
    Animator anim;
    public GameObject player;
    int nowRot;
    bool onground;
    bool down;
    float downtime;
    bool armar;
    public GameObject ball;
	// Use this for initialization
	void Start () {
        nowRot = 1;
        StatasSet();
        rg = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
        DcomTime -= Time.deltaTime;
        if(DcomTime <= 0)
        {
            Dashcount = 0;
        }
        if (down)
        {
            downtime -= Time.deltaTime;
            if (Input.GetKeyDown(KeyCode.Space)||downtime <= 0)
            {
                armar = false;
                down = false;
                anim.SetTrigger("Wake");
            }

            return;
        }
        Attack();
        Move();

        //テスト用
        if (Input.GetKeyDown(KeyCode.Z))
        {
            Instantiate(ball,new Vector3(gameObject.transform.position.x+3f,1f), gameObject.transform.localRotation);
        }
    }
    //各種ステータス設定
    public void StatasSet()
    {
        speed = 2f;
        jumppow = 10f;
    }
    //各種移動
    void Move()
    {
        //歩行計算
        float x = 0f;
        int pasRot = 0;
        anim.SetFloat("Speed", 0f);
        float h = Input.GetAxis("Horizontal_2P");
        float ha = Input.GetAxis("HorizontalAlt_2P");
        float h2 = Input.GetAxisRaw("Horizontal_2P");
        float ha2 = Input.GetAxisRaw("HorizontalAlt_2P");
        if (debug)
        {
            x = Input.GetAxisRaw("Horizontal");
        }
        else if (Mathf.Abs(h) > Mathf.Abs(ha))
        {
            x = h;
        }
        else
        {
            x = ha;
        }

        if(Mathf.Abs(x) <= 0.3)
        {
            x = 0;
        }
        //ジャンプ
        if (Input.GetButtonDown("B_2P"))
        {
            anim.SetTrigger("Jump");
            Debug.Log("ジャンプ");
        }
        //しゃがみ
        if (Input.GetAxisRaw("Vertical_2P")<= -5)
        {
            anim.SetTrigger("Sqwat");
        }
        pasRot = nowRot;
        if (x > 0f)
        {
            nowRot = 1;
        }
        if (x < 0f)
        {
            nowRot = -1;
        }
        if (pasRot != nowRot && x != 0)
        {
            transform.Rotate(new Vector3(0, player.transform.rotation.y + 180 % 360, 0));
            pasRot = nowRot;
        }
        if (nowRot == -1 && x <= 0)
        {
            x *=nowRot;
        }
        
        //ダッシュ
        if (Dashcount == 0)
        {
            if(x == 0)
            {
                Dashcount++;
                DcomTime = 0.5f;
            }
        }
        else if (Dashcount == 1)
        {
            if (x == 1)
            {
                Dashcount++;
                DcomTime = 0.5f;
            }
        }
        else if (Dashcount == 2)
        {
            if (x == 0)
            {
                Dashcount++;
                DcomTime = 0.5f;
            }
        }
        else if(Dashcount == 3)
        {
            if (x == 1)
            {
                Dashcount++;
                dash = true;
            }
        }
        if (dash)
        {
            anim.SetBool("Dash", dash);
            anim.SetFloat("Speed", x * speed * dashspd);
            Vector3 vec = transform.forward * x * speed * dashspd;
            rg.velocity = vec;
        }
        else
        {
            anim.SetFloat("Speed", x * speed);
            Vector3 vec = transform.forward * x * speed;
            rg.velocity = vec;
        }
        if(x == 0&& dash)
        {
            Dashcount = 0;
            dash = false;
            anim.SetBool("Dash", dash);
        }
    }
    //攻撃
    void Attack()
    {
        //弱攻撃
        if (Input.GetButtonDown("X_2P"))
        {
            Debug.Log("弱攻撃");
            anim.SetTrigger("AtackJ");
        }
        //強攻撃
        else if (Input.GetButtonDown("Y_2P"))
        {
            Debug.Log("強攻撃");
            anim.SetTrigger("AtackK");
        }
        //投げ
        else if (Input.GetButtonDown("A_2P"))
        {
            Debug.Log("投げ攻撃");
            anim.SetTrigger("Thrrow");
        }
        //必殺技
        else if(Input.GetAxisRaw("Trigger_2P") >= 0.5)
        {
            Debug.Log("必殺技");
        }
        //ガード
        else if (Input.GetButton("RB_2P"))
        {
            Debug.Log("ガード");
        }
        else if (Input.GetButtonUp("RB_2P"))
        {
            Debug.Log("ガード解除");
        }
    }
    //あたり判定
    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag != "Player" && other.gameObject.tag != "Wall" && !armar)
        {
            armar = true;
            anim.SetTrigger("Down");
            down = true;
            downtime = 3f;
        }
    }
}
